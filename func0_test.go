package testerr

import (
	"errors"
	"testing"
)

func ExampleShould() {
	testedFunction := func() error { return errors.New("this is an error") }

	var t *testing.T                  // Usually passed in by the test framework
	Should(testedFunction()).BeNil(t) // will call t.Error()
}

func ExampleShall() {
	testedFunction := func() error { return errors.New("this is an error") }

	var t *testing.T                 // Usually passed in by the test framework
	Shall(testedFunction()).BeNil(t) // will call t.Fatal()
}

type interr struct{}

func (interr) Error() string { return "internal error" }

func TestShould(t *testing.T) {
	pt := probeTest{t: t}
	Should(errors.New("ERROR")).Any(&pt, NotNil(), Is(interr{}))
	Should(interr{}).All(&pt, NotNil(), Is(interr{}))
	pt.expect(0, 0)
}
