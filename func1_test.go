package testerr

import (
	"errors"
	"testing"
)

func ExampleShould1() {
	testedFunction := func() (int, error) {
		return 4711, errors.New("this is an error")
	}

	var t *testing.T                        // Usually passed in by the test framework
	n := Should1(testedFunction()).BeNil(t) // will call t.Error()
	if n != 4711 {
		t.Errorf("unexpected n == %d", n)
	}
}

func ExampleShall1() {
	testedFunction := func() (int, error) {
		return 4711, errors.New("this is an error")
	}

	var t *testing.T                       // Usually passed in by the test framework
	n := Shall1(testedFunction()).BeNil(t) // will call t.Fatal()
	if n != 4711 {
		t.Errorf("unexpected n == %d", n)
	}
}

func testOKFunc() (int, error) { return 4711, nil }

func testFailFunc() (int, error) { return -4711, errors.New("ERROR") }

func TestShall1_BeNil(t *testing.T) {
	pt := probeTest{t: t}
	Shall1(testOKFunc()).BeNil(&pt)
	pt.expect(0, 0)
	pt.reset()
	Shall1(testFailFunc()).BeNil(&pt)
	pt.expect(0, 1)
}

func TestShall1_All(t *testing.T) {
	pt := probeTest{t: t}
	Shall1(testFailFunc()).All(&pt,
		CheckFunc(func(err error) error {
			if err != nil {
				return errors.New("Wow, that's <nil>")
			}
			return nil
		}),
	)
	pt.expect(0, 1)
}
