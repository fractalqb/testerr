package testerr

import (
	"testing"
)

func Example() {
	var t *testing.T
	testedFunc := func() (int, error) { return 4711, nil }

	// In plain Go:
	n, err := testedFunc()
	if err != nil {
		t.Fatalf("illegal error: %s", err)
	}
	if n < 0 || n > 10000 {
		t.Errorf("%d out of range", n)
	}

	// Short, but ignoring error may yield misleading results:
	n, _ = testedFunc()
	if n < 0 || n > 10000 {
		t.Errorf("%d out of range", n)
	}

	// Using testerr:
	n = Shall1(testedFunc()).BeNil(t) // Calls t.Fatal() if err is not nil
	if n < 0 || n > 10000 {
		t.Errorf("%d out of range", n)
	}
	// Output:
	//
}
