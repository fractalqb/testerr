package testerr

import "errors"

const (
	shouldPrefix = "unexpected:"
	shallPrefix  = "illegal:"
)

// Should records the passed error, usually from a function call, in order to
// react to an unexpected error with t.Error(). The error can be analysed using
// the methods of [Ret0Error].
func Should(err error) Ret0Error { return Ret0Error{err} }

type Ret0Error struct{ err error }

func (r Ret0Error) Check(t Test, check Checker) error {
	if err := check.Check(r.err); err != nil {
		t.Helper()
		t.Error(shouldPrefix, err)
	}
	return r.err
}

func (r Ret0Error) All(t Test, checks ...Checker) error {
	var errs []error
	for _, c := range checks {
		if err := c.Check(r.err); err != nil {
			errs = append(errs, err)
		}
	}
	if len(errs) > 0 {
		t.Helper()
		t.Error(shouldPrefix, errors.Join(errs...))
	}
	return r.err
}

func (r Ret0Error) Any(t Test, checks ...Checker) error {
	var errs []error
	for _, c := range checks {
		if err := c.Check(r.err); err != nil {
			errs = append(errs, err)
		}
	}
	if len(errs) == len(checks) {
		t.Helper()
		t.Error(shouldPrefix, errors.Join(errs...))
	}
	return r.err
}

func (r Ret0Error) BeNil(t Test, msg ...any) error {
	if err := IsNil(msg...).Check(r.err); err != nil {
		t.Helper()
		t.Error(shouldPrefix, err)
	}
	return r.err
}

func (r Ret0Error) NotNil(t Test, msg ...any) error {
	if err := NotNil(msg...).Check(r.err); err != nil {
		t.Helper()
		t.Error(shouldPrefix, err)
	}
	return r.err
}

func (r Ret0Error) Be(t Test, target error) error {
	if err := Is(target).Check(r.err); err != nil {
		t.Helper()
		t.Error(shouldPrefix, err)
	}
	return r.err
}

// Shall records the passed error, usually from a function call, in order to
// react to an unexpected error with t.Fatal(). The error can be analysed using
// the methods of [Ret0Fatal].
func Shall(err error) Ret0Fatal { return Ret0Fatal{err} }

type Ret0Fatal struct{ err error }

func (r Ret0Fatal) Check(t Test, check Checker) error {
	if err := check.Check(r.err); err != nil {
		t.Helper()
		t.Fatal(shallPrefix, err)
	}
	return r.err
}

func (r Ret0Fatal) All(t Test, checks ...Checker) error {
	var errs []error
	for _, c := range checks {
		if err := c.Check(r.err); err != nil {
			errs = append(errs, err)
		}
	}
	if len(errs) > 0 {
		t.Helper()
		t.Fatal(shallPrefix, errors.Join(errs...))
	}
	return r.err
}

func (r Ret0Fatal) Any(t Test, checks ...Checker) error {
	var errs []error
	for _, c := range checks {
		if err := c.Check(r.err); err != nil {
			errs = append(errs, err)
		}
	}
	if len(errs) == len(checks) {
		t.Helper()
		t.Fatal(shallPrefix, errors.Join(errs...))
	}
	return r.err
}

func (r Ret0Fatal) BeNil(t Test, msg ...any) {
	if err := IsNil(msg...).Check(r.err); err != nil {
		t.Helper()
		t.Fatal(shallPrefix, err)
	}
}

func (r Ret0Fatal) NotNil(t Test, msg ...any) error {
	if err := NotNil(msg...).Check(r.err); err != nil {
		t.Helper()
		t.Fatal(shallPrefix, err)
	}
	return r.err
}

func (r Ret0Fatal) Be(t Test, target error) error {
	if err := Is(target).Check(r.err); err != nil {
		t.Helper()
		t.Fatal(shallPrefix, err)
	}
	return r.err
}
