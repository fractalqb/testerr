package testerr

import "errors"

// Should1 records the passed values, usually from a function call, in order to
// react to an unexpected error with t.Error(). The error is evaluated using the
// [Ret1Error] methods and the remaining values are returned.
func Should1[R any](v R, err error) Ret1Error[R] {
	return Ret1Error[R]{res: v, err: err}
}

type Ret1Error[R any] struct {
	res R
	err error
}

func (r Ret1Error[R]) Check(t Test, check Checker) R {
	if err := check.Check(r.err); err != nil {
		t.Helper()
		t.Error(shouldPrefix, err)
	}
	return r.res
}

func (r Ret1Error[R]) All(t Test, checks ...Checker) R {
	var errs []error
	for _, c := range checks {
		if err := c.Check(r.err); err != nil {
			errs = append(errs, err)
		}
	}
	if len(errs) > 0 {
		t.Helper()
		t.Error(shouldPrefix, errors.Join(errs...))
	}
	return r.res
}

func (r Ret1Error[R]) Any(t Test, checks ...Checker) R {
	var errs []error
	for _, c := range checks {
		if err := c.Check(r.err); err != nil {
			errs = append(errs, err)
		}
	}
	if len(errs) > len(checks) {
		t.Helper()
		t.Error(shouldPrefix, errors.Join(errs...))
	}
	return r.res
}

func (r Ret1Error[R]) BeNil(t Test, msg ...any) R {
	if err := IsNil(msg...).Check(r.err); err != nil {
		t.Helper()
		t.Error(shouldPrefix, err)
	}
	return r.res
}

func (r Ret1Error[R]) NotNil(t Test, msg ...any) R {
	if err := NotNil(msg...).Check(r.err); err != nil {
		t.Helper()
		t.Error(shouldPrefix, err)
	}
	return r.res
}

func (r Ret1Error[R]) Be(t Test, target error) R {
	if err := Is(target).Check(r.err); err != nil {
		t.Helper()
		t.Error(shouldPrefix, err)
	}
	return r.res
}

// Shall1 records the passed values, usually from a function call, in order to
// react to an unexpected error with t.Fatal(). The error is evaluated using the
// [Ret1Fatal] methods and the remaining values are returned.
func Shall1[R any](v R, err error) Ret1Fatal[R] {
	return Ret1Fatal[R]{res: v, err: err}
}

type Ret1Fatal[R any] struct {
	res R
	err error
}

func (r Ret1Fatal[R]) Check(t Test, check Checker) R {
	if err := check.Check(r.err); err != nil {
		t.Helper()
		t.Fatal(shallPrefix, err)
	}
	return r.res
}

func (r Ret1Fatal[R]) All(t Test, checks ...Checker) R {
	var errs []error
	for _, c := range checks {
		if err := c.Check(r.err); err != nil {
			errs = append(errs, err)
		}
	}
	if len(errs) > 0 {
		t.Helper()
		t.Fatal(shallPrefix, errors.Join(errs...))
	}
	return r.res
}

func (r Ret1Fatal[R]) Any(t Test, checks ...Checker) R {
	var errs []error
	for _, c := range checks {
		if err := c.Check(r.err); err != nil {
			errs = append(errs, err)
		}
	}
	if len(errs) == len(checks) {
		t.Helper()
		t.Fatal(shallPrefix, errors.Join(errs...))
	}
	return r.res
}

func (r Ret1Fatal[R]) BeNil(t Test, msg ...any) R {
	if err := IsNil(msg...).Check(r.err); err != nil {
		t.Helper()
		t.Fatal(shallPrefix, err)
	}
	return r.res
}

func (r Ret1Fatal[R]) NotNil(t Test, msg ...any) R {
	if err := NotNil(msg...).Check(r.err); err != nil {
		t.Helper()
		t.Fatal(shallPrefix, err)
	}
	return r.res
}

func (r Ret1Fatal[R]) Be(t Test, target error) R {
	if err := Is(target).Check(r.err); err != nil {
		t.Helper()
		t.Fatal(shallPrefix, err)
	}
	return r.res
}
