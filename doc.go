/*
Package testerr provides some helpers to check errors from calls in unit tests.

Especially if the errors are not the focus of the test, it is useful to also
check these errors - especially those accompanying other return values - without
distracting too much from the actual test.

Test functions in testerr that begin with "Should" ([Should], [Should1]) call
the error function of a test if the test fails. Test functions that begin with
"Shall" ([Shall], [Shall1]) call the fatal function of the test if the test
fails.
*/
package testerr
