# testerr
[![Go Reference](https://pkg.go.dev/badge/git.fractalqb.de/fractalqb/testerr.svg)](https://pkg.go.dev/git.fractalqb.de/fractalqb/testerr)

Go testing helpers to check errors from function calls – see [Go reference](https://pkg.go.dev/git.fractalqb.de/fractalqb/testerr).

Basically this:

``` Go
// In plain Go:
n, err := someCallInGoTest() // returns (int, error)
if err != nil {
	t.Fatalf("illegal error: %s", err)
}
if n < 0 || n > 10000 {
	t.Errorf("%d out of range", n)
}

// Short, but ignoring error may yield misleading results:
n, _ := someCallInGoTest()
if n < 0 || n > 10000 {
	t.Errorf("%d out of range", n)
}

// Using testerr:
n := Shall1(someCallInGoTest()).BeNil(t) // Calls t.Fatal() if err != nil
if n < 0 || n > 10000 {
	t.Errorf("%d out of range", n)
}
```
