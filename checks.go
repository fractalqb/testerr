package testerr

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

// A Checker examines the error value passed to the Check method for some
// condition and return nil if the condition is fulfilled. Otherwise, Check
// returns an error that describes why the condition is not fulfilled. Basic
// checkers are already integrated into the RetN(Error|Fatal) types for easy
// use.
type Checker interface{ Check(error) error }

type CheckFunc func(error) error

func (f CheckFunc) Check(err error) error { return f(err) }

// IsNil returns a checker that checks whether an error is nil. With msg you can
// add an information message in case the check fails (see [MsgString]).
func IsNil(msg ...any) Checker {
	if len(msg) == 0 {
		return CheckFunc(func(err error) error {
			if err != nil {
				return fmt.Errorf("non-<nil> error %[1]T='%[1]s'", err)
			}
			return nil
		})
	}
	return CheckFunc(func(err error) error {
		if err != nil {
			return fmt.Errorf("%s: %[2]T='%[2]s'", MsgString(msg...), err)
		}
		return nil
	})
}

// NotNil returns a checker that checks whether an error is not nil. With msg
// you can add an information message in case the check fails (see [MsgString]).
func NotNil(msg ...any) Checker {
	if len(msg) == 0 {
		return CheckFunc(func(err error) error {
			if err == nil {
				return errors.New("<nil> error")
			}
			return nil
		})
	}
	return CheckFunc(func(err error) error {
		if err == nil {
			return fmt.Errorf("<nil> error: %s", MsgString(msg...))
		}
		return nil
	})
}

// Is returns a checker that uses [errors.Is](err, target) to check the error
// err.
func Is(target error) Checker {
	return CheckFunc(func(err error) error {
		if !errors.Is(err, target) {
			return fmt.Errorf("error is not %[1]T='%[1]s', but %[2]T='%[2]s'",
				target,
				err,
			)
		}
		return nil
	})
}

// Msg returns a checker that requires err != nil and checks the error string to
// be [MsgString](msg).
func Msg(msg ...any) Checker {
	return CheckFunc(func(err error) error {
		msgStr := MsgString(msg...)
		if err == nil {
			return fmt.Errorf("<nil> error, expect message '%s'", msgStr)
		} else if msg := err.Error(); msg != msgStr {
			return fmt.Errorf("error message '%s', expect '%s'", msg, msgStr)
		}
		return nil
	})
}

// Msg returns a checker that requires err != nil and checks the error string to
// contain [MsgString](msg).
func MsgContains(msg ...any) Checker {
	return CheckFunc(func(err error) error {
		msgStr := MsgString(msg...)
		if err == nil {
			return fmt.Errorf("<nil> error, expect '%s' in message", msgStr)
		} else if msg := err.Error(); !strings.Contains(msg, msgStr) {
			return fmt.Errorf("error message '%s' does not contain '%s'", msg, msgStr)
		}
		return nil
	})
}

// Msg returns a checker that requires err != nil and checks the error string to
// start with [MsgString](msg).
func MsgPrefix(msg ...any) Checker {
	return CheckFunc(func(err error) error {
		msgStr := MsgString(msg...)
		if err == nil {
			return fmt.Errorf("<nil> error, expect message prefix '%s'", msgStr)
		} else if msg := err.Error(); !strings.HasPrefix(msg, msgStr) {
			return fmt.Errorf("error message '%s' does not start with '%s'", msg, msgStr)
		}
		return nil
	})
}

// Msg returns a checker that requires err != nil and checks the error string to
// end with [MsgString](msg).
func MsgSuffix(msg ...any) Checker {
	msgStr := MsgString(msg...)
	return CheckFunc(func(err error) error {
		if err == nil {
			return fmt.Errorf("<nil> error, expect message suffix '%s'", msgStr)
		} else if msg := err.Error(); !strings.HasSuffix(msg, msgStr) {
			return fmt.Errorf("error message '%s' does not end with '%s'", msg, msgStr)
		}
		return nil
	})
}

// Msg returns a checker that requires err != nil and checks the error string to
// match the regular expression expr.
func MsgMatch(expr *regexp.Regexp) Checker {
	return CheckFunc(func(err error) error {
		msg := err.Error()
		if expr.MatchString(msg) {
			return nil
		}
		return fmt.Errorf("error message '%s' does not match '%s'", msg, expr.String())
	})
}

// Msg returns a checker that requires err != nil and checks the error string to
// match the regular expression [regexp.Compile](expr).
func MsgMatchRegexp(expr string) Checker {
	pat, err := regexp.Compile(expr)
	if err != nil {
		err = fmt.Errorf("match expression error: %w", err)
		return CheckFunc(func(error) error { return err })
	}
	return CheckFunc(func(err error) error {
		msg := err.Error()
		if pat.MatchString(msg) {
			return nil
		}
		return fmt.Errorf("error message '%s' does not match '%s'", msg, pat.String())
	})
}
