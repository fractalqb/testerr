package testerr

import "fmt"

var (
	Ln  lnIndicator
	Fmt fmtIndicator
)

type lnIndicator struct{}

type fmtIndicator struct{}

// MsgString is used to format strings throughout testerr using the fmt.Print
// functions without requiring the …ln() and …f() variants for all functions
// suppoting messages. MsgString will call fmt.Sprintln() if msg[0]==Ln and
// fmt.Sprintf if msg[0]==Fmt. Otherwise MsgString calls fmt.Sprint.
func MsgString(msg ...any) string {
	if len(msg) == 0 {
		return ""
	}
	switch msg[0] {
	case Fmt:
		if f, ok := msg[1].(string); ok {
			return fmt.Sprintf(f, msg[2:]...)
		}
	case Ln:
		return fmt.Sprintln(msg[1:]...)
	}
	return fmt.Sprint(msg...)
}
