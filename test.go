package testerr

// Test requires ony the necessary subset of methods from Go's testing.TB
// interface. This allows us to implement the Test interface to test the testerr
// package itself. Note that testing.TB cannot be implemented outside of the
// testing packge for good reasons.
type Test interface {
	Error(args ...any)
	Fatal(args ...any)
	Helper()
}
