package testerr

import "testing"

type probeTest struct {
	t          testing.TB
	errorCount int
	fatalCount int
}

func (t *probeTest) reset() { t.errorCount, t.fatalCount = 0, 0 }

func (t *probeTest) expect(errorCount, fatalCount int) bool {
	ok := true
	if t.errorCount != errorCount {
		t.t.Errorf("expect %d × error, have %d", errorCount, t.errorCount)
		ok = false
	}
	if t.fatalCount != fatalCount {
		t.t.Errorf("expect %d × fatal, have %d", fatalCount, t.fatalCount)
		ok = false
	}
	return ok
}

func (t *probeTest) Error(args ...any) { t.errorCount++ }

func (t *probeTest) Fatal(args ...any) { t.fatalCount++ }

func (t *probeTest) Helper() {}
