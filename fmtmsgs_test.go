package testerr

import (
	"fmt"
	"io"
	"os"
)

func ExampleMsgString() {
	fmt.Print("fmt", 4711, true)
	fmt.Println()
	io.WriteString(os.Stdout, MsgString("fmt", 4711, true))
	fmt.Println()

	fmt.Println("fmt", 4711, true)
	io.WriteString(os.Stdout, MsgString(Ln, "fmt", 4711, true))

	fmt.Printf("fmt %d & %t", 4711, true)
	fmt.Println()
	io.WriteString(os.Stdout, MsgString(Fmt, "fmt %d & %t", 4711, true))
	fmt.Println()
	// Output:
	// fmt4711 true
	// fmt4711 true
	// fmt 4711 true
	// fmt 4711 true
	// fmt 4711 & true
	// fmt 4711 & true
}
